﻿**Title: Vigenère Cipher**

**Description: Here is the passphrase to my account id. Can you decrypt the text so that I can use the passcode for my project? I have the key for you. Can you please help me decrypt this: hefi{lxtw@kqknpmjtg}**

**Hint**: use the key- CTF 

**Intended Learning and outcome:** Understand the basics of the Vigenère Cipher.

**Solution:**  To decrypt the Vigenère Cipher, we will be using the given key "CTF". The Vigenère Cipher works by replacing each letter in the plaintext with its corresponding letter in the key. Here's is the flag: flag{SSRD@forintern}

