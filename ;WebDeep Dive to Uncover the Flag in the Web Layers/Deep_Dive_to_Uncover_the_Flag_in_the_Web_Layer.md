﻿Category: **Web**

**Title: Deep Dive to Uncover the Flag in the Web Layers**

**Description:**
Please review my page <https://rbstina.github.io/webctf/flag.html>. Make use of your site inspection abilities to dig into the HTML, CSS, and JavaScript elements to find the flag.

**Hint**: Utilize the inspection tool to explore the source codes and files of the client-side.

**Intended Learning and outcome:** Gain fundamental knowledge and experience evaluating a web page and viewing its source codes and files on the client side. This activity will help grasp basic web technology and practice web inspection skills.

**Solution:**  The given link will open up a page where one can find a button saying ‘click me’ to get the flag. When a user clicks the button, the flag is not present. As goes for the hint, we will start inspecting the page source. We find the first part of the flag in the HTML part and as we dig deeper into the CSS and the JavaScript files, we get the remaining of the flags too. flag{exampleOn webctfforinternship@ssrd}.

