﻿**Title: Brewed Secrets: Double Shot** 

**Description**: Journey into Australia’s coffee culture and discover the intensity of its rich espresso. Coffee is the way of life and a gateway to revealing the secrets. Discover the layers in the espresso drink with every sip and you shall find your answer. [IMAGE](https://drive.google.com/file/d/1AHb_NyavtMXZO1R0oZgGzAaQ8LqPovVA/view?usp=drive_link)**


**Hint**: Have you heard of steganography? The key to your secret is hidden in its file.

**Intended Learning and outcome:** 

With the given challenge, I was able to learn about steganography. Tools used: steghide to embed a text file with a main image. Steghide tool is used in various platform to conceal hidden messages from unauthorized access. It can be image, audio file, or a video. This tool is very effective to keep unwanted access as the altered image appears identical to the original image to a normal user. This can be done by using various commands provided by the tool. 
I familiarized myself with the basic command syntax to embed the file with a cover image file ensuring an added layer of security through password protection. The process of extracting is also by using the commands provided and extracting it to the desired file. 

**Solution:** 

An image of coffee is given to solve this challenge. The image looks normal to the eye. But as the hint is on the topic steganography, The tool will be used to extract the valuable information. Also, we need a passphrase to use the tool steghide, the hint denotes the passphrase is hidden in the file. So, we will use ExifTool to check the details on the metadata of the image file coffee.jpg. There we can get the passphrase under comment as “flag”. Then we will use the command steghide extract -sf coverfile -p passphrase -xf anwer.txt. Here, the coverfile is the image used to hide the file and the answer.txt file is where the secret is saved after extraction. The flag is base64 encoded and we decode the file and get the file Flag{stegonography}. 

