﻿**Title: Do not validate using client-side**

**Description:** Oh no! I appear to have forgotten my login credentials. Can you help me get back into my account? To begin the challenge, go to the following website link: <https://rbstina.github.io/Login_webctf/page.html>.

**Hint**: Explore the depths of client-side validation

**Intended Learning and outcome:** It attempts to point out the significance of effective server-side validation in web applications. This challenge is to recognise the security risks that comes with depending entirely on the client-side validation.  It provides an insights on the potential exploitation and attackers easily bypassing the validation and sending malicious code into one’s system. 

**Solution:**  

As we get into the link, we see a plain login page. The page can be inspected to search for any hidden flag. As we navigate into the html file, we learn that it has a CSS and a JavaScript file as well. The username and the password encoded in base64 can be found in the source files and upon logging, the flag is displayed in a pop window as FLAG{webBasedCTFTologin}.

